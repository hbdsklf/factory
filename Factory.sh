#!/bin/bash

# TODO: Cleanup
# TODO: Command I
# TODO: Room Supply
# TODO: All higher level commands (DEF_, q, loop, eloop)
# TODO: Controls
# TODO: Console
# TODO: Editor/Breakpoints

BOTTOM_LINE=20
TOP_LINE=2
CLAW_POS=0
CLAW_DATA=""
MODE="MANUAL"
STEP=false

ROOMS=(
    "Production"
    "Storage space 1"
    "Storage space 2"
    "Storage space 3"
    "Garbage"
    "Shipping"
    "Supply"
    "Invertor"
    "And"
)

ROOM_TYPE_PRODUCTION=1
ROOM_TYPE_STORAGE=2
ROOM_TYPE_GARBAGE=3
ROOM_TYPE_SHIPPING=4
ROOM_TYPE_SUPPLY=5
ROOM_TYPE_INVERTER=6
ROOM_TYPE_AND=7
ROOM_TYPE=(
    "$ROOM_TYPE_PRODUCTION"
    "$ROOM_TYPE_STORAGE"
    "$ROOM_TYPE_STORAGE"
    "$ROOM_TYPE_STORAGE"
    "$ROOM_TYPE_GARBAGE"
    "$ROOM_TYPE_SHIPPING"
    "$ROOM_TYPE_SUPPLY"
    "$ROOM_TYPE_INVERTER"
    "$ROOM_TYPE_AND"
)

ROOM_DATA_PRODUCTION=1
ROOM_DATA_STORAGE=(
    [2]=""
    [3]=""
    [4]=""
)
ROOM_DATA_INVERTER=""
ROOM_DATA_SHIPPING=""
RAM=0

function init {
    # exit on error
    set -e
    trap 'echo "error on line $LINENO"' ERR
    # trap
    trap "tput rmcup; tput sgr0; tput cnorm; stty echo" EXIT
    # new screen
    tput smcup
    # hide cursor
    tput civis
    stty -echo
    initStateTransferSend
}

function initStateTransferSend {
    trap 'rm /dev/shm/$$' EXIT
    echo "" > /dev/shm/$$
}

function initStateTransferReceive {
    trap "receiveState" SIGUSR1
}

function sendState {
    echo "$STEP $MODE" > /dev/shm/$$
    kill -SIGUSR1 "$child"
}

function receiveState {
    IFS=" " read -r -a dat <<< "$(cat /dev/shm/$$)"
    STEP="${dat[0]}"
    MODE="${dat[1]}"
}

function draw_menu {
    tput cup 0 0
    echo " MODE (F6): $MODE  "
    tput cup 0 20
    echo "TIMER: 0.5s"
    tput cup 0 40
    echo "RAM [$RAM]"
}

function mode_change {
    case "$MODE" in
        "MANUAL")
            MODE="AUTO"
            STEP=true
            ;;
        "AUTO")
            MODE="TIMER"
            STEP=true
            ;;
        "TIMER")
            MODE="MANUAL"
            STEP=false
            ;;
    esac
    sendState
    draw_menu
}

function menu_worker {
    while true; do
        if read -rsN1 char; then
            while read -rsN1 -t 0.001 ; do
                char+="${REPLY}"
            done
        fi
        case "$char" in
            $'\e[17~')
                mode_change
                ;;
            a)
                STEP=true
                sendState "$STEP"
                ;;
            *)
                debug "Key $char pressed"
                ;;
        esac
    done
}

# first argument is width
# second argument is text
# third argument is padding character which defaults to space
function centerecho {
    termwidth="$1"
    if [ -n "$3" ]; then
        paddingchar="$3"
    else
        paddingchar=" "
    fi
    padding="$(printf '%0.1s' "$paddingchar"{1..500})"
    printf '%*.*s %s %*.*s' 0 "$(((termwidth-2-${#2})/2))" "$padding" "$2" 0 "$(((termwidth-1-${#2})/2))" "$padding"
}

function draw_rooms {
    # move cursor down
    tput cup $BOTTOM_LINE 0
    # find widest room
    ROOM_WIDTH=0
    for roomNo in ${!ROOMS[*]}; do
        if (( ${#ROOMS[$roomNo]} > ROOM_WIDTH )); then
            ROOM_WIDTH=${#ROOMS[$roomNo]}
        fi
    done
    ROOM_WIDTH=$((ROOM_WIDTH+2))
    # foreach room
    echo -n "|"
    for roomNo in ${!ROOMS[*]}; do
        # draw in the same width
        centerecho $ROOM_WIDTH "${ROOMS[$roomNo]}"
        echo -n "|"
    done

    tput cup $((BOTTOM_LINE-1)) 0
    echo -n "|"
    for roomNo in ${!ROOMS[*]}; do
        centerecho $ROOM_WIDTH ""
        echo -n "|"
    done
    tput cup $((BOTTOM_LINE-2)) 0
    echo -n "|"
    for roomNo in ${!ROOMS[*]}; do
        centerecho $ROOM_WIDTH ""
        echo -n "|"
    done

    draw_room_data
}

function pickdrop {
    roomType=${ROOM_TYPE[$CLAW_POS]}
    case $roomType in
        $ROOM_TYPE_PRODUCTION)
            if [[ "$CLAW_DATA" == "" ]]; then
                CLAW_DATA="$ROOM_DATA_PRODUCTION"
            else
                ROOM_DATA_PRODUCTION="$CLAW_DATA"
            fi
            ;;
        $ROOM_TYPE_STORAGE)
            if [[ "$CLAW_DATA" == "" ]]; then
                CLAW_DATA="${ROOM_DATA_STORAGE[$CLAW_POS]: -1}"
                ROOM_DATA_STORAGE[$CLAW_POS]="${ROOM_DATA_STORAGE[$CLAW_POS]: 0: -1}"
                echo ""
            else
                ROOM_DATA_STORAGE[$CLAW_POS]+="$CLAW_DATA"
                CLAW_DATA=""
            fi
            ;;
        $ROOM_TYPE_GARBAGE)
            CLAW_DATA=""
            ;;
        $ROOM_TYPE_SHIPPING)
            if [[ "$CLAW_DATA" == "" ]]; then
                CLAW_DATA="${ROOM_DATA_SHIPPING: -1}"
                ROOM_DATA_SHIPPING="${ROOM_DATA_SHIPPING: 0: -1}"
            else
                ROOM_DATA_SHIPPING+="$CLAW_DATA"
                CLAW_DATA=""
            fi
            ;;
#ROOM_TYPE_SUPPLY=5
        $ROOM_TYPE_INVERTER)
            if [[ "$CLAW_DATA" == "" ]]; then
                CLAW_DATA="$ROOM_DATA_INVERTER"
                ROOM_DATA_INVERTER=""
            else
                if [[ "$CLAW_DATA" == 1 ]]; then
                    ROOM_DATA_INVERTER=0
                else
                    ROOM_DATA_INVERTER=1
                fi
                CLAW_DATA=""
            fi
            ;;
        $ROOM_TYPE_AND)
            if [[ "$CLAW_DATA" == "" ]]; then
                CLAW_DATA="${ROOM_DATA_AND: -1}"
                ROOM_DATA_AND="${ROOM_DATA_AND: 0: -1}"
            else
                ROOM_DATA_AND+="$CLAW_DATA"
                CLAW_DATA=""
            fi
            if [[ "$ROOM_DATA_AND" =~ [0-1]0|0[0-1] ]]; then
                ROOM_DATA_AND=0
            elif [[ "$ROOM_DATA_AND" == "11" ]]; then
                ROOM_DATA_AND=1
            fi
            ;;
    esac
    draw_room_data
}

# param 1 is data to format
# if param 2 is true, full ascii chars are returned, otherwise the remaining bits
function format_data {
    data="$1"
    fullBytes=$((${#data} / 8))
    if $2; then
        n=0; while (( n++ < fullBytes )); do
            bits="${data:$(( (n-1) * 8 )):8}"
            # shellcheck disable=SC2059
            printf "$(printf '\%o' "$((2#$bits))")"
        done
    else
        echo "${data:$(( fullBytes * 8))}"
    fi
}

function draw_room_data {
    for roomTypeIndex in ${!ROOM_TYPE[*]}; do
        case ${ROOM_TYPE[$roomTypeIndex]} in
            $ROOM_TYPE_PRODUCTION)
                tput cup $((BOTTOM_LINE-2)) $(((ROOM_WIDTH / 2) - 0))
                echo "[$ROOM_DATA_PRODUCTION]"
                ;;
            $ROOM_TYPE_STORAGE)
                tput cup $((BOTTOM_LINE-2)) $(( 1 + ((ROOM_WIDTH + 1) * roomTypeIndex) + ( (ROOM_WIDTH - 10) / 2) ))
                printf "[%0-8s]" "$(format_data "${ROOM_DATA_STORAGE[$roomTypeIndex]}" false)"
                tput cup $((BOTTOM_LINE-1)) $(( 1 + ((ROOM_WIDTH + 1) * roomTypeIndex) + ( (ROOM_WIDTH - 10) / 2) ))
                printf "[%0-8s]" "$(format_data "${ROOM_DATA_STORAGE[$roomTypeIndex]}" true)"
                ;;
            $ROOM_TYPE_SHIPPING)
                tput cup $((BOTTOM_LINE-2)) $(( 1 + ((ROOM_WIDTH + 1) * roomTypeIndex) + ( (ROOM_WIDTH - 10) / 2) ))
                printf "[%0-8s]" "$(format_data "$ROOM_DATA_SHIPPING" false)"
                tput cup $((BOTTOM_LINE-1)) $(( 1 + ((ROOM_WIDTH + 1) * roomTypeIndex) + ( (ROOM_WIDTH - 10) / 2) ))
                printf "[%0-8s]" "$(format_data "$ROOM_DATA_SHIPPING" true)"
                ;;
            $ROOM_TYPE_SUPPLY)
                tput cup $((BOTTOM_LINE-2)) $(( 1 + ((ROOM_WIDTH + 1) * roomTypeIndex) + ( (ROOM_WIDTH - 10) / 2) ))
                printf "[%0-8s]" "$(format_data "$ROOM_DATA_SUPPLY" false)"
                tput cup $((BOTTOM_LINE-1)) $(( 1 + ((ROOM_WIDTH + 1) * roomTypeIndex) + ( (ROOM_WIDTH - 10) / 2) ))
                printf "[%0-8s]" "$(format_data "$ROOM_DATA_SUPPLY" true)"
                ;;
            $ROOM_TYPE_INVERTER)
                tput cup $((BOTTOM_LINE-2)) $(( 1 + ((ROOM_WIDTH + 1) * roomTypeIndex) + ( (ROOM_WIDTH - 2) / 2) ))
                if [[ "$ROOM_DATA_INVERTER" == "" ]]; then
                    echo "[ ]"
                else
                    echo "[$ROOM_DATA_INVERTER]"
                fi
                tput cup $((BOTTOM_LINE-1)) $(( 1 + ((ROOM_WIDTH + 1) * roomTypeIndex) + ( (ROOM_WIDTH - 1) / 2) ))
                echo -n "!"
                ;;
            $ROOM_TYPE_AND)
                tput cup $((BOTTOM_LINE-2)) $(( 1 + ((ROOM_WIDTH + 1) * roomTypeIndex) + ( (ROOM_WIDTH - 3) / 2) ))
                if [[ "$ROOM_DATA_AND" == "" ]]; then
                    echo "[ ]"
                else
                    echo "[$ROOM_DATA_AND]"
                fi
                tput cup $((BOTTOM_LINE-1)) $(( 1 + ((ROOM_WIDTH + 1) * roomTypeIndex) + ( (ROOM_WIDTH - 1) / 2) ))
                echo -n "&"
                ;;
        esac
    done
}

function debug {
    tput cup $(( BOTTOM_LINE + 2)) 0
    echo "$1"
}

# first param is room number
# second param is claw state (0 = closed/empty, 1 = open, 2 = closed/full)
function draw_claw {
    # clear drawn claw
    n=$TOP_LINE; while (( n++ < ( BOTTOM_LINE - 3 ) )); do
        tput cup $n 0
        tput el
    done

    # cleanup claw position
    CLAW_POS=$1
    if [ "$CLAW_POS" -lt 0 ]; then
        CLAW_POS=0
    elif (( CLAW_POS >= ${#ROOMS[*]} )); then
        CLAW_POS=$((${#ROOMS[*]} - 1))
    fi

    clawPos=$((CLAW_POS * ( ROOM_WIDTH + 1) + (ROOM_WIDTH / 2) + 1))
    #debug "$CLAW_POS / ${#ROOMS[*]} / $clawPos"
    clawLength=3
    n=0; while (( n++ < clawLength )); do
        tput cup $((TOP_LINE + n)) $clawPos
        echo -n "|"
    done
    tput cup $((TOP_LINE + n)) $((clawPos - 1))
    echo -n "/ \\"
    tput cup $((TOP_LINE + n + 1)) $((clawPos - 2))
    if [[ "$CLAW_DATA" == "" ]]; then
        echo -n " \\ /"
    else
        echo -n " |$CLAW_DATA|"
    fi
    #if (( $2 == 0 )); then
        #echo -n " \\ /"
    #elif (( $2 == 1 )); then
        #echo -n "/   \\"
    #elif (( $2 == 2 )); then
        #echo -n " | |"
    #fi
}

function parse {
    initStateTransferReceive
    booted=false
    while read -r line; do
        if ! $booted; then
            case $line in
                /*)
                    # comment, ignore
                    ;;
                BOOT)
                    booted=true
                    ;;
            esac
        else
            while read -r -n 1 chr; do
                case $chr in
                    /)
                        # comment, ignore
                        break
                        ;;
                    "<")
                        CLAW_POS=$((CLAW_POS - 1))
                        draw_claw $CLAW_POS 0
                        ;;
                    ">")
                        CLAW_POS=$((CLAW_POS + 1))
                        draw_claw $CLAW_POS 0
                        ;;
                    "v")
                        pickdrop
                        draw_claw $CLAW_POS 0
                        ;;
                    "O")
                        debug "$(format_data "$ROOM_DATA_SHIPPING" true)"
                        ROOM_DATA_SHIPPING=""
                        draw_room_data
                        ;;
                    "^")
                        if [[ "$CLAW_DATA" == "" ]]; then
                            if [[ "$RAM" == 0 ]]; then
                                RAM=1
                            else
                                RAM=0
                            fi
                        else
                            RAM="$CLAW_DATA"
                            CLAW_DATA=""
                        fi
                        draw_menu
                        ;;
                    "I")
                        tput cup $(( BOTTOM_LINE + 3)) 0
                        echo -n "INPUT: "
                        read -r input
                        debug "$input"
                        draw_room_data
                esac
                case "$MODE" in
                    "MANUAL")
                        while [[ $STEP != true ]]; do
                            sleep 0.1
                        done
                        STEP=false
                        ;;
                    "TIMER")
                        sleep 0.5
                        ;;
                esac

            done <<< "$line"
        fi
    done <"$1"
}

init
draw_menu
draw_rooms
draw_claw $CLAW_POS 0
parse "$1" &
child=$!
menu_worker
sleep 50
